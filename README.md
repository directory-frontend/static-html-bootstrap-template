********************************************************************************
presentation link: https://www.youtube.com/watch?v=I-4ZKJ-dgp
********************************************************************************



The main goal of this task is to prepare full-fledged, static templates. It is realistically a development of the warm-up exercise from the first classes.
After class one, you should already have two templates:
list of products
detailed view

In this task we will focus on their development.
Your templates should include:
Logo
Top navigation
Part for content
footer

The site should be fully responsive. However, for simplicity, we will only accept 4 resolutions:
Full HD (1920x1080)
1280x1240
tablet
smartphone

The check will take place using the emulator built into the browser.

Additional guidelines:
Logo in the form of a graphic file. The logo and website colors must be consistent
Navigation must contain several basic items such as: home page, about us, contact, products. Additional items are allowed and should be consistent with the theme of your site
Page content
In the case of a template with products, the content should contain the first page of your products, e.g. 10 different ones. Each product should contain a photo, title, short description, categories, price, additional buttons necessary for your website to function in the future. Include a link to product details. This template should have a two-column layout in a 1: 3 ratio
For a template with product details, this is a view that focuses on the product and provides additional information about the product. It can be a one-column system
The footer should be built in accordance with current standards, i.e. have information about the company, auxiliary navigation and additional things such as subscription to the newsletter

When building your templates, please remember to have tags such as <nav>, <content>, <fig>, <figcaption>